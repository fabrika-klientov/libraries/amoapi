<?php
/**
 * @category  AquaBox
 * @package   Binotel
 * @author    Sergei Yarmolyuk <sergei.yarmolyuk@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.4.7
 * @link      https://fabrika-klientov.ua
 */

namespace Ufee\Amo\Methods\IncomingLeads;

class IncomingLeadsExec extends \Ufee\Amo\Base\Methods\Post
{
	protected 
		$url = '/api/v2/incoming_leads/accept';

    /**
     * @param $id
     * @param $responsible_user_id
     * @return \Ufee\Amo\Base\Methods\Collection
     */
    public function exec($id, $responsible_user_id)
    {
        return $this->call(['accept' => [$id], 'user_id' => $responsible_user_id]);
    }
}
