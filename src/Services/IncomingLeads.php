<?php
/**
 * @category  AquaBox
 * @package   Binotel
 * @author    Sergei Yarmolyuk <sergei.yarmolyuk@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.7.2
 * @link      https://fabrika-klientov.ua
 */

namespace Ufee\Amo\Services;

use Ufee\Amo\Base\Services\Traits;

class IncomingLeads extends \Ufee\Amo\Base\Services\MainEntity
{
    use Traits\SearchByName, Traits\SearchByPhone;

    protected static $_require = [
	    'add' => ['source_name'],
	    'exec' => ['user_id', 'accept'],
	];
    protected $methods = ['add', 'list', 'exec'];
    protected
        $entity_key = 'incomingLeads',
        $entity_model = '\Ufee\Amo\Models\IncomingLead',
        $entity_collection = '\Ufee\Amo\Collections\IncomingLeadCollection',
        $cache_time = false;

    /**
     * Get full
     * @return Collection
     */
    public function incomingLeads()
    {
	return $this->list->recursiveCall();
    }

    /**
     * @param $id
     * @param $responsible_user_id
     * @return mixed
     */
    public function exec($id, $responsible_user_id)
    {
        return $this->exec->exec($id, $responsible_user_id);
    }
}